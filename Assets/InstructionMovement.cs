﻿using UnityEngine;
using UnityEngine.XR.WSA.Input;

public class InstructionMovement : MonoBehaviour {

    // Target object for the instructions to face
    public GameObject camera;

	// Use this for initialization
	void Start () {
        InteractionManager.InteractionSourcePressed += GetPosition;
	}
	
	// Update is called once per frame
	void Update () {

        // Makes the instructions always face the player
        this.transform.LookAt(camera.transform);
        this.transform.Rotate(Vector3.up, 180);
    }

    private void GetPosition(InteractionSourcePressedEventArgs state)
    {
        var interactionSourceState = state.state;
        Vector3 pos;
        if (interactionSourceState.sourcePose.TryGetPosition(out pos))
        {
            updatePanelPosition(pos);
        }
    }

    private void updatePanelPosition(Vector3 newPos)
    {
        this.transform.position = newPos;
    }
}
