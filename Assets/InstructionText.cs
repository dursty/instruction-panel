﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.WSA.Input;

public class InstructionText : MonoBehaviour {

    // Holds the default size of the text
    public TextMesh instructionText;

	// Use this for initialization
	void Start () {

        // Calculates the max number of characters on a line
        int maxCharacter = (int) (getPanelWidth() / getCharacterWidth());
        Debug.Log("maxCharacter: " + maxCharacter);

        // Creates a new text using the maxCharacter
        string testWord = fixOverflow("This is a test instruction to test overflow. If it works this word should be wraped correctly.", maxCharacter);

        // For placing text on quad
        var textObject = Instantiate(instructionText);
        var rend = gameObject.GetComponent<Renderer>();

        Vector3 textPos = new Vector3(rend.bounds.min.x, rend.bounds.max.y, rend.bounds.max.z);

        textObject.transform.parent = gameObject.transform;
        textObject.transform.position = textPos;
        textObject.text = testWord;
    }
	
    // Adds new line if character count goes over the character per line.
    public string fixOverflow(string instruction, int maxCharPerLine)
    {
        string newString = "";

        string[] wordList = instruction.Split();

        int charCount = 0;

        foreach (string word in wordList)
        {
            int wordLength = word.Length + 1;

            if (charCount + wordLength > maxCharPerLine)
            {
                newString += '\n';
                charCount = 0;
            }

            newString += word + ' ';
            charCount += wordLength;
        }
        return newString;
    }

    // Gets the width of one character
    private float getCharacterWidth()
    {
        // sets instruction text to the largest character W
        instructionText.text = "W";
        var bounds = instructionText.GetComponent<Renderer>().bounds;
        Debug.Log("Char width: "+ (bounds.max.x - bounds.min.x));

        // Returns the max and min of render bounds for one character
        return bounds.max.x - bounds.min.x;
    }

    private float getPanelWidth()
    {
        var rend = gameObject.GetComponent<Renderer>();

        return rend.bounds.max.x - rend.bounds.min.x;

    }

}
